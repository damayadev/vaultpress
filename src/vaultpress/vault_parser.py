import datetime
import pathlib
import re
import uuid

import markdown
from pydantic import BaseModel

from .util import latex_to_mathml, mermaid_diagram_to_svg


class VaultNote(BaseModel):
    metadata: dict = {}
    tags: list[str] = []
    url: str | None = None
    html: str | None = None
    mtime: datetime.datetime | None = None
    post_title: str | None = None
    post_category: str | None = None
    html_output_file: str | None = None
    attachments: list[str] = []
    backlinks: list[str] = []


class Vault(BaseModel):
    notes: dict[str, list[VaultNote]] = {}
    files: list[pathlib.Path] = []
    _items: list[VaultNote] = []
    backlinks: dict = {}


class VaultParser:
    def __init__(self, config: dict) -> None:
        self.vault_directory = config["vault_directory"]
        self.vault_attachments_directory = config["vault_attachments_directory"]
        self.web_assets_directory = config["web_assets_directory"]
        self.web_notes_directory = config["web_notes_directory"]
        self.existing_files = self._get_existing_web_files()
        self.vault: Vault = self._load_vault()

    def _get_existing_web_files(self):
        existing_files = {}
        for file in pathlib.Path(self.web_notes_directory).rglob("*.html"):
            # f"/{self.web_notes_directory}/{vault_item.post_category}/{vault_item.mtime}/{vault_item.post_title}"
            file = pathlib.Path(str(file).replace(self.web_notes_directory + "/", ""))
            post_title = file.parts[-1]
            post_category = file.parts[:-2]
            existing_files[f"{post_category}/{post_title}"] = file
        return existing_files

    def _get_post_category(self, path: pathlib.Path) -> str:
        if self.vault_directory.startswith("./"):
            path = pathlib.Path(str(path).replace(self.vault_directory.lstrip("./"), ""))
        else:
            path = pathlib.Path(str(path).replace(self.vault_directory, ""))

        return '/'.join(path.parts[:-1]).lstrip("/")

    def _resolve_links(self) -> None:
        # In the document we replace all [[link]] with the correct url
        # we replace all attachments with the correct url
        # we add all backlinks to the item

        for vault_item in self.vault._items:
            for attachment in vault_item.attachments:
                attachment_name = attachment.rsplit("|")[0]
                if attachment_name in self.vault.files:
                    link = self.web_assets_directory + "/" + attachment_name
                    vault_item.html = re.sub(
                        rf"\[{{2}}{attachment}]{{2}}",
                        f'<a href="{link}">{attachment_name}</a>',
                        vault_item.html
                    )
                elif attachment_name in self.vault.notes:
                    if len(self.vault.notes[attachment_name]) > 1:
                        pass
                    note = self.vault.notes[attachment_name][0]
                    href = f"/{self.web_notes_directory}/{note.post_category}/{note.mtime}/{note.post_title}"
                    if f"{note.post_category}/{note.post_title}" in self.existing_files:
                        href = str(self.existing_files[f"{note.post_category}/{note.post_title}"])
                    vault_item.html = re.sub(
                        rf"\[{{2}}{attachment}]{{2}}",
                        f'<a href="{href}">{attachment_name}</a>',
                        vault_item.html
                    )
            backlinks = []
            if vault_item.post_title in self.vault.backlinks:
                for item in self.vault.backlinks[vault_item.post_title]:
                    href = f"/{self.web_notes_directory}/{item.post_category}/{item.mtime}/{item.post_title}"
                    if f"{item.post_category}/{item.post_title}" in self.existing_files:
                        href = str(self.existing_files[f"{item.post_category}/{item.post_title}"])
                    backlinks.append(href)
            vault_item.backlinks = backlinks


    def _start_callout(self, callout_type, callout_title=None):
        return [
            '<div data-callout="info" class="callout">',
            '<div class="callout-title">Can callouts be nested?</div>',
            '<div class="callout-content">'
        ]

    def _parse_callouts(self, content):
        output_content = []
        callout_html = []
        callout_types = {
            "info": "info",
            "note": "note",
            "todo": "todo",
            "bug": "bug",
            "example": "example",
            "abstract": "abstract",
            "summary": "abstract",
            "tldr": "abstract",
            "tip": "tip",
            "hint": "tip",
            "important": "tip",
            "success": "success",
            "check": "success",
            "done": "success",
            "question": "question",
            "help": "question",
            "faq": "question",
            "warning": "warning",
            "caution": "warning",
            "attention": "warning",
            "failure": "failure",
            "fail": "failure",
            "missing": "failure",
            "danger": "danger",
            "error": "danger",
            "quote": "quote",
            "cite": "quote",
        }
        # you can go down but not up

        callout_re = "|".join(callout_types.keys())
        callout_level = 0
        callout_level_uuid = {}
        callout_content = []
        for line in content.split("\n"):
            if m := re.match(rf"(>+) \[!({callout_re}](.*)", line):
                callout_level = m.group(1).count(">")
                callout_level_uuid[callout_level] = uuid.uuid4().hex
                callout_html.extend(self._start_callout(m.group(2), m.group(3)))

            elif m := re.match(rf"(>+)", line) and callout_level > 0:
                this_callout_level = m.group(1).count(">")
                if this_callout_level < callout_level:
                    callout_html.append("<p>" + "<br/>".join(callout_content) + "</p>")
                    callout_html.append('</div></div>')
                    callout_level = this_callout_level

            elif m := re.match(rf"(>+) (.*)", line) and callout_level > 0:
                this_callout_level = m.group(1).count(">")
                content = ">" * (this_callout_level - callout_level) + m.group(2)
                callout_content.append(markdown.markdown(content))
            else:
                if callout_level > 0:
                    callout_content.append(line)
                    callout_html.append("<p>" + "<br/>".join(callout_content) + "</p>")
                    callout_html.append('</div></div>')
                callout_level = 0
        return '\n'.join(output_content)

    def _load_vault(self) -> Vault:
        """
        Metadata:
            title: I will survive this parsing
            output_filename: philosophy/foo/bar.html
            created: 2020-05-27
            last_updated: 2020-05-27
            category: foo/bar/baz
        :param self:
        :return:
        """
        vault = Vault()

        if self.vault_attachments_directory is not None:
            for file in pathlib.Path(self.vault_attachments_directory).rglob("*"):
                if not file.is_file():
                    continue
                if file.suffix != ".md":
                    vault.files.append(file)
                    continue

        for file in pathlib.Path(self.vault_directory).rglob("*"):
            if not file.is_file():
                continue

            if file.suffix != ".md":
                vault.files.append(file)
                continue

            vault_item = VaultNote()
            vault_item.post_category = self._get_post_category(file)
            vault_item.post_title = file.stem
            vault_item.mtime = datetime.datetime.fromtimestamp(file.lstat().st_mtime).strftime('%Y-%m-%d')

            with open(file) as f:
                contents = f.read()

                raw_tags = re.findall(r"#([\w-]+)", contents)
                for tag in raw_tags:
                    if tag.isdigit():
                        continue
                    vault_item.tags.append(tag)
                    contents = contents.replace(f"#{tag}", "")
                contents = re.sub(r"\s{2,}", " ", contents)

                mm_placeholder_to_svg = {}
                for item in re.finditer(r"```mermaid(.*?)```", contents, re.MULTILINE | re.DOTALL):
                    mermaid_svg = mermaid_diagram_to_svg(item.group(1))
                    placeholder = uuid.uuid4().hex
                    contents = contents.replace(item.group(), placeholder)
                    mm_placeholder_to_svg[placeholder] = mermaid_svg

                md = markdown.Markdown(extensions=["sane_lists", "toc", "meta", "codehilite", "extra", "smarty"])
                markdown_html = md.convert(contents)

                for placeholder, svg in mm_placeholder_to_svg.items():
                    markdown_html.replace(placeholder, svg)

                vault_item.metadata = md.Meta
                vault_item.html = markdown_html

                for item in re.finditer(r"\$\$(.*?)\$\$", vault_item.html, re.MULTILINE | re.DOTALL):
                    vault_item.html = vault_item.html.replace(item.group(1), latex_to_mathml(item.group(1)))

                for item in re.finditer(r"\[{2}(.*?)]{2}", vault_item.html):
                    vault.backlinks.setdefault(item.group(1), set()).add(vault_item)
                    vault_item.attachments.append(item.group(1))

                for item in re.finditer(r"%%(.*?)%%", vault_item.html, re.MULTILINE | re.DOTALL):
                    vault_item.html = vault_item.html.replace(item.group(0), "")

            item_path = f"{vault_item.post_category}/{vault_item.post_title}"
            vault.notes.setdefault(item_path, []).append(vault_item)
            vault.notes.setdefault(vault_item.post_title, []).append(vault_item)
            vault._items.append(vault_item)

        self._resolve_links()
        return vault
