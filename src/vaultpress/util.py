import subprocess
import tempfile

import latex2mathml.converter


def latex_to_mathml(latex: str) -> str:
    return latex2mathml.converter.convert(latex)


def mermaid_diagram_to_svg(mm_diagram: str) -> str:
    with tempfile.NamedTemporaryFile() as mm_input_tf, tempfile.NamedTemporaryFile() as mm_output_tf:
        with open(mm_input_tf.name, "w") as f:
            f.write(mm_diagram)
        subprocess.run(["mmdc", "-i", mm_input_tf.name, "--outputFormat", "svg", "--output", mm_output_tf.name])
        with open(mm_output_tf.name) as f:
            svg = f.read()
    return svg
