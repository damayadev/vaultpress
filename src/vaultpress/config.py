import pathlib
from os.path import join as join_path

import yaml
from platformdirs import user_config_dir


DEFAULT_CONFIG_FILE = pathlib.Path((user_config_dir('vaultpress'), 'vaultpress.yml'))
ALTERNATIVE_CONFIG_FILE = pathlib.Path(join_path(user_config_dir('vaultpress.yml')))

if not DEFAULT_CONFIG_FILE.is_file() and ALTERNATIVE_CONFIG_FILE.is_file():
    DEFAULT_CONFIG_FILE = ALTERNATIVE_CONFIG_FILE

if not DEFAULT_CONFIG_FILE.is_file():
    DEFAULT_CONFIG_FILE.parent.mkdir(parents=True, exist_ok=True)
    DEFAULT_CONFIG_FILE.touch()

DEFAULTS = {
    "theme": "hello_css",
    "vault_directory": "",
    "vault_attachments_directory": "",
    "web_notes_directory": "",
    "web_assets_directory": ""
}

def load_config(config_path=DEFAULT_CONFIG_FILE):

    if isinstance(config_path, str):
        config_path = pathlib.Path(config_path)

    if not config_path.is_file():
        raise FileNotFoundError(f"{config_path} is not a file")

    with open(config_path) as fconfig:
        config = yaml.safe_load(fconfig)

    for k, v in DEFAULTS.items():
        config[k] = v

    return config
