"""Command-line interface."""
import typer

# Flow
# Settings:
#   Configure: Vault directory, html directory, choose theme
#   Git: init? existing
# Configure
# Generate
# Publish

app = typer.Typer()


@app.command()
def init(
    config_file: Annotated[str, typer.Option("-c", "--config-file")],
    git_url: Annotated[str, typer.Option("")],
    project_name: Annotated[str, typer.Option()]
):
    pass


@app.command()
def generate(
    config_file: Annotated[str, typer.Option("-c", "--config-file")],
    input_directory: Annotated[str, typer.Option("-c", "--config-file")],
    output_directory: Annotated[str, typer.Option("-c", "--config-file")],
    theme: Annotated[str, typer.Option("-c", "--config-file")],
):
    pass


@app.publish()
def publish(
    config_file: Annotated[str, typer.Option("-c", "--config-file")]
):
    pass


if __name__ == "__main__":
    app()